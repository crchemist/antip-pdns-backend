#ifndef REACTOR_H
#define REACTOR_H

#include <iostream>
#include <string>
#include <istream>

#include <boost/bind.hpp>
#include <boost/asio.hpp>

#include <pqxx/pqxx>

#include "protocol.h"

using namespace std;
namespace asio = boost::asio;
namespace posix = boost::asio::posix;

class Reactor
{
    public:
        Reactor();
        virtual ~Reactor();

        asio::io_service io_service;
        posix::stream_descriptor *in, *out;
        pqxx::work *txn;
        pqxx::connection *conn;

        void Start();
        void Stop();

        void HandleRequest(const boost::system::error_code& error);
    protected:
    private:
        asio::streambuf input_buffer;
};


#endif // REACTOR_H
