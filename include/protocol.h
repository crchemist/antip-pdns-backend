#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <string>
#include <iostream>
#include <ostream>
#include <fstream>

#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <boost/foreach.hpp>
#include <boost/asio/ip/address.hpp>
#include <boost/algorithm/string.hpp>

#include <pqxx/pqxx>
#include <pqxx/util>
#include <pqxx/isolation>

#define HELO_CMD "HELO"
#define Q_CMD "Q"
#define AXFR_CMD "AXFR"

#define OK_MSG "OK\tANTIP backend fired out\n"
#define FAIL_MSG "FAIL\n"
#define END_MSG "END\n"

#define QTYPE_A "A"
#define QTYPE_ANY "ANY"
#define QTYPE_CNAME "CNAME"
#define QTYPE_SOA "SOA"


//TODO: determine the meaning of "id" field.
#define A_ANSWER_TEMPLATE "DATA\t%s\t%s\tA\t6\t-1\t%s\n"

using namespace std;
using boost::format;
using boost::io::group;

namespace asio = boost::asio;
namespace ip = asio::ip;


class Protocol
{
    public:
        Protocol(istream *input, pqxx::work *txn);
        virtual ~Protocol();

        string type, qname, qclass, qtype, remote_ip, local_ip;
        pqxx::work *txn;
        int version;
        int id;

        string Answer();
    protected:
    private:
        string Finish(string response);
};

#endif // PROTOCOL_H
