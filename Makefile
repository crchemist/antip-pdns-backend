CC = g++
CFLAGS = -O3 -lpqxx -lpq -lboost_program_options -lboost_filesystem -lboost_iostreams -lboost_system
OUTPUT = bin/antip-pdns-backend

all:
	$(CC) $(CFLAGS) -o $(OUTPUT) -I include/  main.cpp src/protocol.cpp src/reactor.cpp
	strip $(OUTPUT)
