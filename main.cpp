#include "reactor.h"

#include <boost/program_options.hpp>

namespace po = boost::program_options;

int main(int argc, char *argv[])
{
    int dbport;
    string dbhost;
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message(options are not supported by Powerdns :( )")
        ("dbname", po::value<string>(), "db name")
        ("dbuser", po::value<string>(), "db username")
        ("dbpassword", po::value<string>(), "db password (not implemented yet)")
        ("dbhost", po::value<string>(&dbhost)->default_value(string("localhost")), "db host (not yet implemented)")
        ("dbport", po::value<int>(&dbport)->default_value(5432), "db port (not yet implemented)");

    po::variables_map var_map;
    po::store(po::parse_command_line(argc, argv, desc), var_map);
    po::notify(var_map);

    if (var_map.count("help")) {
        cout << desc << endl;
        return 1;
    }

    Reactor reactor;
    reactor.Start();

    return 0;
}
