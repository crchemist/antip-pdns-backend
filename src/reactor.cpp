#include "reactor.h"
#include "protocol.h"

#include <fstream>

Reactor::Reactor() {
    //ctor
    this->in = new posix::stream_descriptor(this->io_service, dup(STDIN_FILENO));
    this->out = new posix::stream_descriptor(this->io_service, dup(STDOUT_FILENO));
    this->conn = new pqxx::connection("dbname=antiporgua user=antip password=em0ngvf8d5m host=localhost");
    this->txn = new pqxx::work(*conn);

}

Reactor::~Reactor() {
    //dtor
}

void Reactor::Start() {
    // All types of requests is contained in one line
    asio::async_read_until(*(this->in), this->input_buffer, "\n",
        boost::bind(&Reactor::HandleRequest, this, _1));
    io_service.run();
}

void Reactor::Stop() {
    io_service.stop();
}

void Reactor::HandleRequest(const boost::system::error_code& error) {
    if (error == boost::asio::error::eof) {
        this->Stop();
        return;
    };

    istream is(&this->input_buffer);
    Protocol *protocol = new Protocol(&is, this->txn);
    if (protocol->type != "") {
        string answer = protocol->Answer();
        this->out->write_some(asio::buffer(answer, answer.length()));
    }

    delete protocol;
    // handle next request
    asio::async_read_until(*(this->in), this->input_buffer, "\n",
        boost::bind(&Reactor::HandleRequest, this, _1));
}
