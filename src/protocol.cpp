#include "protocol.h"

using namespace std;

Protocol::Protocol(istream *input, pqxx::work *txn)
{
    this->txn = txn;
    *input >> this->type;
    if (type == HELO_CMD) {
        *input >> this->version;
    } else {
        *input >> this->qname >> this->qclass >> this->qtype >> this->id
               >> this->remote_ip >> this->local_ip;
    }
}

Protocol::~Protocol()
{
    //dtor
}

string Protocol::Answer() {
    if (this->type == HELO_CMD)
        return OK_MSG;

    pqxx::result db_result;
    string response = "";
    cerr << "TYPE: " << this->type;
    if (this->type == Q_CMD) {
        cerr << " QTYPE: " << this->qtype << endl;
        if (this->qtype == QTYPE_SOA || this->qtype == QTYPE_A || this->qtype == QTYPE_ANY) {
            // check for blocked domains first
            db_result = this->txn->exec("SELECT blocked_domains.id FROM blocked_domains "
                "INNER JOIN ips ON blocked_domains.user_id = ips.user_id "
                "WHERE ips.ip = '" + this->remote_ip + "' AND "
                "blocked_domains.name = '" + this->qname + "'");
            if (db_result.size() != 0) {
                response = str(format(A_ANSWER_TEMPLATE) % this->qname
                    % this->qclass
                    % string("74.125.232.212"));
            }

            // check for local domains
            db_result = this->txn->exec("SELECT local_domains.ip FROM local_domains "
                "INNER JOIN ips ON ips.ip = '" + pqxx::sqlesc(this->remote_ip) +
                "' WHERE local_domains.name = '" + pqxx::sqlesc(this->qname) + "'");
            if (db_result.size() != 0) {
                response = str(format(A_ANSWER_TEMPLATE) % this->qname
                    % this->qclass
                    % db_result[0][0].as<string>());
            }
        }
    }
    return this->Finish(response);
}

string Protocol::Finish(string response) {
    if (response != FAIL_MSG)
        return response + END_MSG;
    cerr << "Response: " << response;
    return response;
}
